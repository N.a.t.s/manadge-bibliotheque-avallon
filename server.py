import sqlite3
from flask import Flask

conn = sqlite3.connect('example.db')

app = Flask(__name__)

"""
	Consignes : 

	La bibliothèque municipale d'Avallon fait un appel d'offres pour une application qui permet de gérer son inventaire de livres.

	Le but est de pouvoir s'inscrire en tant que membre, rechercher un livre dans l'inventaire, pouvoir le réserver, 
	voir sa disponibilité ainsi que son emplacement dans la bibliothèque.

	Le personnel de la bibliothèque doit pouvoir ajouter un livre, 
	suivre les réservations, et définir l'emplacement du livre au sein de la bibliothèque.

	Le but est de créer la structure du back-end en Python et les prototypes de fonction.
"""

""" 
	C'est mieux d'utiliser les sessions pour l'authentification 
	que flask gère déjà mais je n'ai pas eu le temps de google 
	et de l'appliquer
"""


@app.route('/auth/register', methods=["POST"])
def auth_register(email: str, password: str) -> str:
	"""
		allow registration of a new user.
	"""

	# if email exists in database 
		# return an error
	
	# else
		# insert new user in database
		# generate token

	# return token
	return"user register successful"

@app.route('/auth/login', methods=["POST"])
def auth_login(email: str, password: str) -> str:
	"""
		allow the login of a user
	"""

	# if email doesn't exists in database
		# return error

	# if password doesn't match email in database
		# return error

	# generate token
	# return token
	return"user login successful"

@app.route('/admin/auth/register', methods=["POST"])
def admin_auth_register(email: str, password: str, secret_key: str) -> str:
	"""
		allow the registration of an employee of the library
	"""

	# if email exists in database 
		# return an error

	# if secret_key is incorrect
		# return an error	

	# insert new user in database
	# generate token
	# return token
	return"admin register successful"

@app.route('/admin/auth/login', methods=["POST"])
def admin_auth_login(email: str, password: str) -> str:
	"""
		allow the login of an employee of the library
	"""

	# if email doesn't exists in database
		# return error

	# if password doesn't match email in database
		# return error

	# generate token
	# return token
	return"admin login successful"

@app.route('/book/search', methods=["GET"])
def book_search(title=None, author=None, category=None) -> object:
	"""
		return a book based of the following criterias :
			title : title of the book
			author: author of the book
			category : category of the book
	"""

	# check if the user is connected using the session id
	# check which parameter has been filled
	# find book in database
	# return book object
	return"book found successfully"

@app.route('/book/<int:id>/details', methods=["GET"])
def book_details(id: int) -> object:
	"""
		return the following details about a book given the id :
			title : title of the book
			author: author of the book
			category : category of the book
			availability : availability of the book | this should be a list
			location : location of the book in the library
	"""

	# check if the user is connected using the session id
	# find the book in database based on id
	# return the details as described
	return"book details found successfully"

@app.route('/book/<int:id>/location', methods=["GET"])
def book_location(id: int) -> object:
	"""
		return the location of the book
	"""

	# check if the user is connected using the session id
	# find the book in database based on id
	# return the details as described
	return"book location found successfully"

@app.route('/book/<int:id>/availability', methods=["GET"])
def book_availability(id: int) -> bool:
	"""
		return the availability of the book
	"""

	# check if the user is connected using the session id
	# find the book in database based on id
	# return true or false
	return"book availability found successfully"

@app.route('/book/<int:id>/setaside', methods=["POST"])
def book_setaside(id: int) -> bool:
	"""
		set a book aside
	"""

	# check if the user is connected using the session id
	# add the user id to the waiting list of the book
	# return true
	return"book successfully set aside"

@app.route('/admin/book/create', methods=["POST"])
def book_create(title: str, author: str, category: str, availability: bool, location: str) -> bool:
	"""
		create a book in database
	"""

	# check if the user is connected
	# check if the user is an admin
	# insert the book in database
	return"book successfully created"

@app.route('/admin/book/<int:id>/waiting_list', methods=["GET"])
def book_waiting_list(id) -> object:
	"""
		retrieves the waiting list of the book
	"""

 	# check if the user is connected
	# check if the user is an admin
	# find the book in database based on id
	# return the waiting list of the book
	return "book waiting list successfully retrieved"

@app.route('/admin/book/<int:id>/location/set', methods=["POST"])
def book_location_set(id, location) -> bool:
	"""
		sets the location of the book to the parameter location
	"""

 	# check if the user is connected
	# check if the user is an admin
	# find the book in database based on id
	# modify the location of the book to the parameter location
	return "book location successfully set"
